from setuptools import setup


setup(
      name="GloHole",
      version="0.1.0",
      packages=["glohole"],
      url="https://bitbucket.org/protocypher/ma-glohole",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A data mining application for Lightroom databases."
)

