package com.smt.lrl.dao;

import com.smt.lrl.domain.DevelopSettings;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;

public class DevelopSettingsMapper implements RowMapper<DevelopSettings> {
    private final boolean exported;

    public DevelopSettingsMapper(final boolean exported) {
        this.exported = exported;
    }


    public DevelopSettings mapRow(ResultSet resultSet, int index) throws SQLException {
        DevelopSettings settings = new DevelopSettings();

        settings.setExported(exported);
        settings.setFilename(getFilename(resultSet));
        settings.setExtension(getExtension(resultSet));
        settings.setSettings(getSettings(resultSet));
        settings.setImageId(getImageId(resultSet));
        settings.setPick(getPick(resultSet));
        settings.setRating(getRating(resultSet));
        settings.setDate(getDate(resultSet));
        settings.setTime(getTime(resultSet));

        return settings;
    }

    private String getFilename(ResultSet rs) {
        try { return rs.getString("filename"); }
        catch(SQLException ignored) { return ""; }
    }

    private String getExtension(ResultSet rs) {
        try { return rs.getString("extension"); }
        catch(SQLException ignored) { return ""; }
    }

    private String getSettings(ResultSet rs) {
        try { return rs.getString("settings"); }
        catch(SQLException ignored) { return ""; }
    }

    private Integer getImageId(ResultSet rs) {
        try {
            int value = rs.getInt("image_id");
            return rs.wasNull() ? null : value;
        } catch(SQLException ignored) { return null; }
    }

    private Integer getPick(ResultSet rs) {
        try {
            int value = rs.getInt("pick");
            return rs.wasNull() ? null : value;
        } catch(SQLException ignored) { return null; }
    }

    private Integer getRating(ResultSet rs) {
        try {
            int value = rs.getInt("rating");
            return rs.wasNull() ? null : value;
        } catch(SQLException ignored) { return null; }
    }

    private String getDatetime(ResultSet rs) {
        try  {
            String text = rs.getString("step");

            int start = text.indexOf('(');
            int end = text.indexOf(')');

            if(start < 0 || end < 0) { return null; }

            return text.substring(start + 1, end);
        } catch(SQLException ignored) {
            return null;
        }
    }

    private LocalTime getTime(ResultSet rs) {
        String datetime = getDatetime(rs);
        if(datetime ==  null) { return null; }

        String[] tokens = datetime.split(" ");

        if(tokens.length != 2) { return null; }

        String[] times = tokens[1].split(":");
        if(times.length != 3) { return null; }

        int hour = Integer.parseInt(times[0]);
        int minute = Integer.parseInt(times[1]);
        int second = Integer.parseInt(times[2]);

        return LocalTime.of(hour, minute, second);
    }

    // Export - Hard Drive (8/31/17 18:23:44)
    private LocalDate getDate(ResultSet rs) {
        String datetime = getDatetime(rs);
        if(datetime ==  null) { return null; }

        String[] tokens = datetime.split(" ");

        if(tokens.length != 2) { return null; }

        String[] dates = tokens[0].split("/");
        if(dates.length != 3) { return null; }

        int month = Integer.parseInt(dates[0]);
        int day = Integer.parseInt(dates[1]);
        int year = Integer.parseInt(dates[2]) + 2000;

        return LocalDate.of(year, month, day);
    }
}
