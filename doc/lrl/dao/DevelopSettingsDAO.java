package com.smt.lrl.dao;

import com.smt.lrl.domain.DevelopSettings;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.List;

public class DevelopSettingsDAO {
    private final static String SQL_EXPORT_DEVELOP_SETTINGS
        = " SELECT i.id_local image_id, d.name step, f.baseName filename, f.extension, i.pick, i.rating, d.text settings "
        + "   FROM Adobe_libraryImageDevelopHistoryStep d "
        + "   JOIN Adobe_images i ON d.image = i.id_local "
        + "   JOIN AgLibraryFile f ON i.rootfile = f.id_local "
        + "  WHERE name LIKE '%export%' "
        + "  ORDER BY f.baseName, d.dateCreated ASC "
        ;

    private final static  String SQL_CURRENT_DEVELOP_SETTINGS
        = " SELECT i.id_local image_id,     '' step, f.baseName filename, f.extension, i.pick, i.rating, d.text settings "
        + " FROM Adobe_imageDevelopSettings d "
        + " JOIN Adobe_images i ON d.image = i.id_local "
        + " JOIN AgLibraryFile f ON i.rootfile = f.id_local "
        + "  ORDER BY f.baseName "
        ;

    private final JdbcOperations jdbc;

    public DevelopSettingsDAO(final DataSource dataSource) {
        jdbc = new JdbcTemplate(dataSource);
    }

    public List<DevelopSettings> getCurrentSettings() {
        List<DevelopSettings> result = jdbc.query(SQL_CURRENT_DEVELOP_SETTINGS, new DevelopSettingsMapper(false));

        if(result == null) {
            return Collections.emptyList();
        } else {
            return result;
        }
    }

    public List<DevelopSettings> getExportedSettings() {
        List<DevelopSettings> result = jdbc.query(SQL_EXPORT_DEVELOP_SETTINGS, new DevelopSettingsMapper(true));

        if(result == null) {
            return Collections.emptyList();
        } else {
            return result;
        }
    }
}
