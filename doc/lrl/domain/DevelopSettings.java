package com.smt.lrl.domain;

import com.smt.lrl.service.KeyValueParser;

import java.time.LocalDate;
import java.time.LocalTime;

public class DevelopSettings {
    private boolean exported;
    private String filename;
    private String extension;
    private String settings;
    private Integer imageId;
    private Integer pick;
    private Integer rating;
    private LocalDate date;
    private LocalTime time;

    private DevelopSettingsDetails details;

    public boolean isExported() { return exported; }
    public void setExported(boolean exported) { this.exported = exported; }

    public Integer getImageId() { return imageId; }
    public void setImageId(Integer imageId) { this.imageId = imageId; }

    public LocalDate getDate() { return date; }
    public void setDate(LocalDate date) { this.date = date; }

    public LocalTime getTime() { return time; }
    public void setTime(LocalTime time) { this.time = time; }

    public String getFilename() { return filename; }
    public void setFilename(String filename) { this.filename = filename; }

    public String getExtension() { return extension; }
    public void setExtension(String extension) { this.extension = extension; }

    public Integer getPick() { return pick; }
    public void setPick(Integer pick) { this.pick = pick; }

    public Integer getRating() { return rating; }
    public void setRating(Integer rating) { this.rating = rating; }

    public String getSettings() { return settings; }
    public void setSettings(String settings) { this.settings = settings; }

    public DevelopSettingsDetails getDetails() {
        if(details == null) {
            details = new DevelopSettingsDetails(KeyValueParser.parse(settings));
        } return details;
    }

    @Override
    public String toString() {
        return "DevelopSettings{"
            + "filename='" + filename + "', "
            + "extension='" + extension + "', "
            + "imageId=" + imageId + ", "
            + "pick=" + pick + ", "
            + "rating=" + rating + ", "
            + "date=" + date + ", "
            + "time=" + time + ", "
            + "}"
            ;
    }
}
