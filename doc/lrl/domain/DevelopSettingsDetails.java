package com.smt.lrl.domain;

import java.util.Map;

public class DevelopSettingsDetails {

    // Basic: White Balance
    private String whiteBalance;           // WhiteBalance      (Basic/White Balance)
    private String whiteBalanceTemp;       // Temperature       (Basic/White Balance/Temperature)
    private String whiteBalanceTint;       // Tint              (Basic/White Balance/Tint)
    private String whiteBalanceCustomTemp; // CustomTemperature (Basic/White Balance/Custom/Temperature)
    private String whiteBalanceCustomTint; // CustomTint        (Basic/White Balance/Custom/Tint)

    // Basic: Tone
    private String toneExposure;   // Exposure2012   (Basic/Tone/Exposure)
    private String toneConstrast;  // Contrast2012   (Basic/Tone/Contrast)
    private String toneHighlights; // Highlights2012 (Basic/Tone/Highlights)
    private String toneShadows;    // Shadows2012    (Basic/Tone/Shadows)
    private String toneWhites;     // Whites2012     (Basic/Tone/Whites)
    private String toneBlacks;     // Blacks2012     (Basic/Tone/Blacks)

    // Basic: Presence
    private String presenceClarity;    // Clarity2012 (Basic/Presence/Clarity)
    private String presenceVibrance;   // Vibrance    (Basic/Presence/Vibrance)
    private String presenceSaturation; // Saturation  (Basic/Presence/Saturation)

    // Tone Curve
    private String toneCurveName;       // ToneCurveName2012    (Tone Curve/Name)
    private String toneCurveCurve;      // ToneCurvePV2012      (Tone Curve/Curve)
    private String toneCurveHighlights; // ParametricHighlights (Tone Curve/Highlights)
    private String toneCurveLights;     // ParametricLights     (Tone Curve/Lights)
    private String toneCurveDarks;      // ParametricDarks      (Tone Curve/Darks)
    private String toneCurveShadows;    // ParametricShadows    (Tone Curve/Shadows)
    private String toneCurveBlue;       // ToneCurvePV2012Blue  (Tone Curve/Blue)
    private String toneCurveGreen;      // ToneCurvePV2012Green (Tone Curve/Green)
    private String toneCurveRed;        // ToneCurvePV2012Red   (Tone Curve/Red)

    // Color: Hue
    private String hueRed;     // HueAdjustmentRed     (Color/Hue/Red)
    private String hueOrange;  // HueAdjustmentOrange  (Color/Hue/Orange)
    private String hueYellow;  // HueAdjustmentYellow  (Color/Hue/Yellow)
    private String hueGreen;   // HueAdjustmentGreen   (Color/Hue/Green)
    private String hueAqua;    // HueAdjustmentAqua    (Color/Hue/Aqua)
    private String hueBlue;    // HueAdjustmentBlue    (Color/Hue/Blue)
    private String huePurple;  // HueAdjustmentPurple  (Color/Hue/Purple)
    private String hueMagenta; // HueAdjustmentMagenta (Color/Hue/Magenta)

    // Color: Saturation
    private String saturationRed;     // SaturationAdjustmentRed     (Color/Saturation/Red)
    private String saturationOrange;  // SaturationAdjustmentOrange  (Color/Saturation/Orange)
    private String saturationYellow;  // SaturationAdjustmentYellow  (Color/Saturation/Yellow)
    private String saturationGreen;   // SaturationAdjustmentGreen   (Color/Saturation/Green)
    private String saturationAqua;    // SaturationAdjustmentAqua    (Color/Saturation/Aqua)
    private String saturationBlue;    // SaturationAdjustmentBlue    (Color/Saturation/Blue)
    private String saturationPurple;  // SaturationAdjustmentPurple  (Color/Saturation/Purple)
    private String saturationMagenta; // SaturationAdjustmentMagenta (Color/Saturation/Magenta)

    // Color: Luminance
    private String luminanceRed;     // LuminanceAdjustmentRed     (Color/Luminance/Red)
    private String luminanceOrange;  // LuminanceAdjustmentOrange  (Color/Luminance/Orange)
    private String luminanceYellow;  // LuminanceAdjustmentYellow  (Color/Luminance/Yellow)
    private String luminanceGreen;   // LuminanceAdjustmentGreen   (Color/Luminance/Green)
    private String luminanceBlue;    // LuminanceAdjustmentBlue    (Color/Luminance/Aqua)
    private String luminanceAqua;    // LuminanceAdjustmentAqua    (Color/Luminance/Blue)
    private String luminancePurple;  // LuminanceAdjustmentPurple  (Color/Luminance/Purple)
    private String luminanceMagenta; // LuminanceAdjustmentMagenta (Color/Luminance/Magenta)

    // Split Toning
    private String splitToneHighlightHue;        // SplitToningHighlightHue        (Split Toning/Highlights/Hue)
    private String splitToneHighlightSaturation; // SplitToningHighlightSaturation (Split Toning/Highlights/Saturation)
    private String splitToneBalance;             // SplitToningBalance             (Split Toning/Balance)
    private String splitToneShadowHue;           // SplitToningShadowHue           (Split Toning/Shadows/Hue)
    private String splitToneShadowSaturation;    // SplitToningShadowSaturation    (Split Toning/Shadows/Saturation)

    // Detail: Sharpening
    private String sharpeningAmount;  // Sharpness          (Detail/Sharpening/Amount)
    private String sharpeningRadius;  // SharpenRadius      (Detail/Sharpening/Radius)
    private String sharpeningDetail;  // SharpenDetail      (Detail/Sharpening/Detail)
    private String sharpeningMasking; // SharpenEdgeMasking (Detail/Sharpening/Masking)

    // Detail: Noise Reduction
    private String noiseReductionLuminance; // LuminanceSmoothing              (Noise Reduction/Luminance)
    private String noiseReductionLDetail;   // LuminanceNoiseReductionDetail   (Noise Reduction/Detail)
    private String noiseReductionContrast;  // LuminanceNoiseReductionContrast (Noise Reduction/Contrast)
    private String noiseReductionColor;     // ColorNoiseReduction             (Noise Reduction/Color)
    private String noiseReductionCDetail;   // ColorNoiseReductionDetail       (Noise Reduction/Detail)
    private String noiseReductionSmooth;    // ColorNoiseReductionSmoothness   (Noise Reduction/Smoothing)

    // Effects: Post Crop Vignetting
    private String vignettingAmount;    // PostCropVignetteAmount            (Effects/Post Crop Vignetting/Amount)
    private String vignettingMidpoint;  // PostCropVignetteMidpoint          (Effects/Post Crop Vignetting/Midpoint)
    private String vignettingRoundness; // PostCropVignetteRoundness         (Effects/Post Crop Vignetting/Roundness)
    private String vignettingFeather;   // PostCropVignetteFeather           (Effects/Post Crop Vignetting/Feather)
    private String vignettingHighlight; // PostCropVignetteHighlightContrast (Effects/Post Crop Vignetting/Highlights)

    // Effects: Grain
    private String grainAmount;    // GrainAmount    (Effects/Grain/Amount)
    private String grainSize;      // GrainSize      (Effects/Grain/Size)
    private String grainRoughness; // GrainFrequency (Effects/Grain/Roughness)

    // Effects: Dehaze
    private String effectsDehaze; // Dehaze   (Effects/Dehaze/Amount)

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    DevelopSettingsDetails(final Map<String,String> parsed) {
        whiteBalance = parsed.get("WhiteBalance");
        whiteBalanceTemp = parsed.get("Temperature");
        whiteBalanceTint = parsed.get("Tint");
        whiteBalanceCustomTemp = parsed.get("CustomTemperature");
        whiteBalanceCustomTint = parsed.get("CustomTint");

        toneExposure = parsed.get("Exposure2012");
        toneConstrast = parsed.get("Contrast2012");
        toneHighlights = parsed.get("Highlights2012");
        toneShadows = parsed.get("Shadows2012");
        toneWhites = parsed.get("Whites2012");
        toneBlacks = parsed.get("Blacks2012");

        presenceClarity = parsed.get("Clarity2012");
        presenceVibrance = parsed.get("Vibrance");
        presenceSaturation = parsed.get("Saturation");

        toneCurveName = parsed.get("ToneCurveName2012");
        toneCurveCurve = parsed.get("ToneCurvePV2012");
        toneCurveHighlights = parsed.get("ParametricHighlights");
        toneCurveLights = parsed.get("ParametricLights");
        toneCurveDarks = parsed.get("ParametricDarks");
        toneCurveShadows = parsed.get("ParametricShadows");
        toneCurveBlue = parsed.get("ToneCurvePV2012Blue");
        toneCurveGreen = parsed.get("ToneCurvePV2012Green");
        toneCurveRed = parsed.get("ToneCurvePV2012Red");

        hueRed = parsed.get("HueAdjustmentRed");
        hueOrange = parsed.get("HueAdjustmentOrange");
        hueYellow = parsed.get("HueAdjustmentYellow");
        hueGreen = parsed.get("HueAdjustmentGreen");
        hueAqua = parsed.get("HueAdjustmentAqua");
        hueBlue = parsed.get("HueAdjustmentBlue");
        huePurple = parsed.get("HueAdjustmentPurple");
        hueMagenta = parsed.get("HueAdjustmentMagenta");

        saturationRed = parsed.get("SaturationAdjustmentRed");
        saturationOrange = parsed.get("SaturationAdjustmentOrange");
        saturationYellow = parsed.get("SaturationAdjustmentYellow");
        saturationGreen = parsed.get("SaturationAdjustmentGreen");
        saturationAqua = parsed.get("SaturationAdjustmentAqua");
        saturationBlue = parsed.get("SaturationAdjustmentBlue");
        saturationPurple = parsed.get("SaturationAdjustmentPurple");
        saturationMagenta = parsed.get("SaturationAdjustmentMagenta");

        luminanceRed = parsed.get("LuminanceAdjustmentRed");
        luminanceOrange = parsed.get("LuminanceAdjustmentOrange");
        luminanceYellow = parsed.get("LuminanceAdjustmentYellow");
        luminanceGreen = parsed.get("LuminanceAdjustmentGreen");
        luminanceBlue = parsed.get("LuminanceAdjustmentBlue");
        luminanceAqua = parsed.get("LuminanceAdjustmentAqua");
        luminancePurple = parsed.get("LuminanceAdjustmentPurple");
        luminanceMagenta = parsed.get("LuminanceAdjustmentMagenta");

        splitToneHighlightHue = parsed.get("SplitToningHighlightHue");
        splitToneHighlightSaturation = parsed.get("SplitToningHighlightSaturation");
        splitToneBalance = parsed.get("SplitToningBalance");
        splitToneShadowHue = parsed.get("SplitToningShadowHue");
        splitToneShadowSaturation = parsed.get("SplitToningShadowSaturation");

        sharpeningAmount = parsed.get("Sharpness");
        sharpeningRadius = parsed.get("SharpenRadius");
        sharpeningDetail = parsed.get("SharpenDetail");
        sharpeningMasking = parsed.get("SharpenEdgeMasking");

        noiseReductionLuminance = parsed.get("LuminanceSmoothing");
        noiseReductionLDetail = parsed.get("LuminanceNoiseReductionDetail");
        noiseReductionContrast = parsed.get("LuminanceNoiseReductionContrast");
        noiseReductionColor = parsed.get("ColorNoiseReduction");
        noiseReductionCDetail = parsed.get("ColorNoiseReductionDetail");
        noiseReductionSmooth = parsed.get("ColorNoiseReductionSmoothness");

        vignettingAmount = parsed.get("PostCropVignetteAmount");
        vignettingMidpoint = parsed.get("PostCropVignetteMidpoint");
        vignettingRoundness = parsed.get("PostCropVignetteRoundness");
        vignettingFeather = parsed.get("PostCropVignetteFeather");
        vignettingHighlight = parsed.get("PostCropVignetteHighlightContrast");

        grainAmount = parsed.get("GrainAmount");
        grainSize = parsed.get("GrainSize");
        grainRoughness = parsed.get("GrainFrequency");

        effectsDehaze = parsed.get("Dehaze");
    }

    public String getWhiteBalance() {
        return whiteBalance;
    }

    public String getWhiteBalanceTemp() {
        return whiteBalanceTemp;
    }

    public String getWhiteBalanceTint() {
        return whiteBalanceTint;
    }

    public String getWhiteBalanceCustomTemp() {
        return whiteBalanceCustomTemp;
    }

    public String getWhiteBalanceCustomTint() {
        return whiteBalanceCustomTint;
    }

    public String getToneExposure() {
        return toneExposure;
    }

    public String getToneConstrast() {
        return toneConstrast;
    }

    public String getToneHighlights() {
        return toneHighlights;
    }

    public String getToneShadows() {
        return toneShadows;
    }

    public String getToneWhites() {
        return toneWhites;
    }

    public String getToneBlacks() {
        return toneBlacks;
    }

    public String getPresenceClarity() {
        return presenceClarity;
    }

    public String getPresenceVibrance() {
        return presenceVibrance;
    }

    public String getPresenceSaturation() {
        return presenceSaturation;
    }

    public String getToneCurveName() {
        return toneCurveName;
    }

    public String getToneCurveCurve() {
        return toneCurveCurve;
    }

    public String getToneCurveHighlights() {
        return toneCurveHighlights;
    }

    public String getToneCurveLights() {
        return toneCurveLights;
    }

    public String getToneCurveDarks() {
        return toneCurveDarks;
    }

    public String getToneCurveShadows() {
        return toneCurveShadows;
    }

    public String getToneCurveBlue() {
        return toneCurveBlue;
    }

    public String getToneCurveGreen() {
        return toneCurveGreen;
    }

    public String getToneCurveRed() {
        return toneCurveRed;
    }

    public String getHueRed() {
        return hueRed;
    }

    public String getHueOrange() {
        return hueOrange;
    }

    public String getHueYellow() {
        return hueYellow;
    }

    public String getHueGreen() {
        return hueGreen;
    }

    public String getHueAqua() {
        return hueAqua;
    }

    public String getHueBlue() {
        return hueBlue;
    }

    public String getHuePurple() {
        return huePurple;
    }

    public String getHueMagenta() {
        return hueMagenta;
    }

    public String getSaturationRed() {
        return saturationRed;
    }

    public String getSaturationOrange() {
        return saturationOrange;
    }

    public String getSaturationYellow() {
        return saturationYellow;
    }

    public String getSaturationGreen() {
        return saturationGreen;
    }

    public String getSaturationAqua() {
        return saturationAqua;
    }

    public String getSaturationBlue() {
        return saturationBlue;
    }

    public String getSaturationPurple() {
        return saturationPurple;
    }

    public String getSaturationMagenta() {
        return saturationMagenta;
    }

    public String getLuminanceRed() {
        return luminanceRed;
    }

    public String getLuminanceOrange() {
        return luminanceOrange;
    }

    public String getLuminanceYellow() {
        return luminanceYellow;
    }

    public String getLuminanceGreen() {
        return luminanceGreen;
    }

    public String getLuminanceBlue() {
        return luminanceBlue;
    }

    public String getLuminanceAqua() {
        return luminanceAqua;
    }

    public String getLuminancePurple() {
        return luminancePurple;
    }

    public String getLuminanceMagenta() {
        return luminanceMagenta;
    }

    public String getSplitToneHighlightHue() {
        return splitToneHighlightHue;
    }

    public String getSplitToneHighlightSaturation() {
        return splitToneHighlightSaturation;
    }

    public String getSplitToneBalance() {
        return splitToneBalance;
    }

    public String getSplitToneShadowHue() {
        return splitToneShadowHue;
    }

    public String getSplitToneShadowSaturation() {
        return splitToneShadowSaturation;
    }

    public String getSharpeningAmount() {
        return sharpeningAmount;
    }

    public String getSharpeningRadius() {
        return sharpeningRadius;
    }

    public String getSharpeningDetail() {
        return sharpeningDetail;
    }

    public String getSharpeningMasking() {
        return sharpeningMasking;
    }

    public String getNoiseReductionLuminance() {
        return noiseReductionLuminance;
    }

    public String getNoiseReductionLDetail() {
        return noiseReductionLDetail;
    }

    public String getNoiseReductionContrast() {
        return noiseReductionContrast;
    }

    public String getNoiseReductionColor() {
        return noiseReductionColor;
    }

    public String getNoiseReductionCDetail() {
        return noiseReductionCDetail;
    }

    public String getNoiseReductionSmooth() {
        return noiseReductionSmooth;
    }

    public String getVignettingAmount() {
        return vignettingAmount;
    }

    public String getVignettingMidpoint() {
        return vignettingMidpoint;
    }

    public String getVignettingRoundness() {
        return vignettingRoundness;
    }

    public String getVignettingFeather() {
        return vignettingFeather;
    }

    public String getVignettingHighlight() {
        return vignettingHighlight;
    }

    public String getGrainAmount() {
        return grainAmount;
    }

    public String getGrainSize() {
        return grainSize;
    }

    public String getGrainRoughness() {
        return grainRoughness;
    }

    public String getEffectsDehaze() {
        return effectsDehaze;
    }
}
