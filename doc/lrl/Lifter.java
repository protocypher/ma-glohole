package com.smt.lrl;

import com.smt.lrl.dao.DevelopSettingsDAO;
import com.smt.lrl.domain.DevelopSettings;
import com.smt.lrl.service.MarkdownReportService;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

public class Lifter {

    private static void printUsage() {
        System.out.println(
              "Lr Lifter Help:\n"
            + "Execution Command: java -jar lr-lifter.jar (1)(2)\n"
            + "Execution Phrase: <cmd> context catalog\n"
            + "Where:\n"
            + "    context - Either 'exported' to display the settings of images when exported, or\n"
            + "              'current' to display the current settings of all images\n"
            + "    catalog - The path to the Lightroom Catalog to process\n\n"
            + "    (1) This assumes the name of the jar is 'lr-lifter.jar'\n"
            + "    (2) Consider creating a shell script or batch file for this\n"
        );
    }

    public static void main(String ... args)  {
        if(args.length != 2) {
            printUsage();
            return;
        }

        String context = args[0];
        String catalog = args[1];

        if(!context.equalsIgnoreCase("exported") && !context.equalsIgnoreCase("current")) {
            System.err.println("<context> must be either 'exported' or 'current'");
            return;
        }

        DevelopSettingsDAO dao;

        try {
            DataSource dataSource = new DriverManagerDataSource("jdbc:sqlite:" + catalog);
            dao = new DevelopSettingsDAO(dataSource);
        } catch(Throwable ignored) {
            System.err.println("<catalog> must be a valid path to a Lightroom Catalog");
            return;
        }

        List<DevelopSettings> settings;

        if(context.equalsIgnoreCase("exported")) {
            settings = dao.getExportedSettings();
        } else {
            settings = dao.getCurrentSettings();
        }

        MarkdownReportService.report(settings);
    }
}
