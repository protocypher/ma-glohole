package com.smt.lrl.service;

import com.smt.lrl.domain.DevelopSettings;
import com.smt.lrl.domain.DevelopSettingsDetails;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static java.util.Objects.deepEquals;
import static java.util.Objects.nonNull;

public class MarkdownReportService {

    // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    public static void report(List<DevelopSettings> settings) {
        for(DevelopSettings s : settings) {
            printImageHeader(s);
            DevelopSettingsDetails d = s.getDetails();

            if(d == null) {
                System.out.format("    * No details available for this image");
                return;
            }

            printBasics(d);
            printToneCurve(d);
            printColor(d);
            printSplitTone(d);
            printDetail(d);
            printEffects(d);
        }
    }

    // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    private static void printImageHeader(DevelopSettings s) {
        String exported = s.isExported() ? "X" : " ";

        String filename = s.getFilename() + "." + s.getExtension();

        String timestamp = "Current";
        if(s.getDate() != null && s.getTime() != null) { timestamp = s.getDate() + " " + s.getTime(); }

        String picked = " ";
        Integer p = s.getPick();
        if(p != null && p != 0) { picked = (p < 0) ? "-" : "+"; }

        String rating = "     ";
        if(s.getRating() != null) {
            switch(s.getRating()) {
                case 1: rating = "    ☼"; break;
                case 2: rating = "   ☼☼"; break;
                case 3: rating = "  ☼☼☼"; break;
                case 4: rating = " ☼☼☼☼"; break;
                case 5: rating = "☼☼☼☼☼"; break;
            }
        }

        System.out.format("- [%s] %s (%s) {%s} <%s>%n", exported, filename, timestamp, picked, rating);
    }

    // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    private static String indent(int level, String line) {
        level *= 4;
        return format("%" + level + "s %s", "*", line);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String either(String value, String alternate) {
        return (value == null) ? alternate : value;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static void printSection(String header, List<String> lines) {
        if(header == null || lines == null) {
            System.out.println("<<Invalid Section>>");
            return;
        }

        if(!lines.isEmpty()) {
            System.out.println(indent(1, "**" + header + "**"));

            for(String line : lines) {
                System.out.println(indent(2, line));
            }
        }
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static void add(List<String> lines, String line) {
        if(lines == null) { return; }
        if(line != null) { lines.add(line); }
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static boolean allNull(String ... values) {
        for(String value : values) {
            if(value != null) { return false; }
        }

        return true;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static boolean anyNull(String ... values) {
        for(String value : values) {
            if(value == null) { return true; }
        }

        return false;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static boolean allEqual(String test, String ... values) {
        if(test == null) { return allNull(values); }

        for(String value : values) {
            if(!test.equals(value)) { return false; }
        }

        return true;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static boolean anyEqual(String test, String ... values) {
        if(test == null) { return anyNull(values); }

        for(String value : values) {
            if(test.equals(value)) { return true; }
        }

        return false;
    }

    // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    private static void printBasics(DevelopSettingsDetails d) {
        List<String> lines = new ArrayList<>();

        add(lines, getWhiteBalance(d));
        add(lines, getCustomWhiteBalance(d));
        add(lines, getTone(d));
        add(lines, getPresence(d));

        printSection("Basic", lines);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getWhiteBalance(DevelopSettingsDetails d) {
        String wb = d.getWhiteBalance();
        String temp = d.getWhiteBalanceTemp();
        String tint = d.getWhiteBalanceTint();

        if(allNull(wb, temp, tint)) { return null; }

        if(nonNull(wb) && anyNull(temp, tint)) {
            return format("White Balance(%s)", wb);
        } else {
            return format(
                "White Balance(%s): Temp(%s) Tint(%s)",
                wb,
                either(temp, "0"),
                either(tint, "0")
            );
        }
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getCustomWhiteBalance(DevelopSettingsDetails d) {
        String temp = d.getWhiteBalanceCustomTemp();
        String tint = d.getWhiteBalanceCustomTint();

        if(anyNull(temp, tint)) { return null; }

        return format(
            "Custom White Balance: Tempe(%s) Tint(%s)",
            either(temp, "0"),
            either(tint, "0")
        );
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getTone(DevelopSettingsDetails d) {
        String exposure = d.getToneExposure();
        String contrast = d.getToneConstrast();
        String highlights = d.getToneHighlights();
        String shadows = d.getToneShadows();
        String whites = d.getToneWhites();
        String blacks = d.getToneBlacks();

        if(allNull(exposure, contrast, highlights, shadows, whites, blacks)) { return null; }

        return format(
            "Tone: Exposure(%s) Contrast(%s) Highlights(%s) Shadows(%s) Whites(%s) Blacks(%s)",
            either(exposure, "0.00"),
            either(contrast, "0"),
            either(highlights, "0"),
            either(shadows, "0"),
            either(whites, "0"),
            either(blacks, "0")
        );
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getPresence(DevelopSettingsDetails d) {
        String clarity = d.getPresenceClarity();
        String vibrance = d.getPresenceVibrance();
        String saturation = d.getPresenceSaturation();

        if(allNull(clarity, vibrance, saturation)) { return null; }

        return format(
            "Presence: Clarity(%s) Vibrance(%s) Saturation(%s)",
            either(clarity, "0"),
            either(vibrance, "0"),
            either(saturation, "0")
        );
    }

    // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    private static void printToneCurve(DevelopSettingsDetails d) {
        List<String> lines = new ArrayList<>();

        add(lines, getToneCurve(d));
        add(lines, getToneCurveShades(d));
        add(lines, getToneCurveColors(d));

        printSection("Tone Curve", lines);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getToneCurve(DevelopSettingsDetails d) {
        String name = d.getToneCurveName();
        String curve = d.getToneCurveCurve();

        if(allNull(name, curve)) { return null; }

        // This is the default state
        if(name.equals("\"Linear\"") && curve.equals("{ 0, 0, 255, 255 }")) { return null; }

        return format(
            "Name(%s) Curve(%s)",
            either(name, "Unknown"),
            either(curve, "?")
        );
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getToneCurveShades(DevelopSettingsDetails d) {
        String highlights = d.getToneCurveHighlights();
        String lights = d.getToneCurveLights();
        String darks = d.getToneCurveDarks();
        String shadows = d.getToneCurveShadows();

        if(allNull(highlights, lights, darks, shadows)) { return null; }

        return format(
            "Highlights(%s) Lights(%s) Darks(%s) Shadows(%s)",
            either(highlights, "0"),
            either(lights, "0"),
            either(darks, "0"),
            either(shadows, "0")
        );
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getToneCurveColors(DevelopSettingsDetails d) {
        String blue = d.getToneCurveBlue();
        String green = d.getToneCurveGreen();
        String red = d.getToneCurveRed();

        if(allNull(blue, green, red)) { return null; }

        if(allEqual("{ 0, 0, 255, 255 }", blue, green, red)) { return null; }

        return format(
            "Blue(%s) Green(%s) Red(%s)",
            either(blue, "0"),
            either(green, "0"),
            either(red, "0")
        );
    }

    // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    private static void printColor(DevelopSettingsDetails d) {
        List<String> lines = new ArrayList<>();

        add(lines, getColorHue(d));
        add(lines, getColorSaturation(d));
        add(lines, getColorLuminance(d));

        printSection("Color", lines);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getColorHue(DevelopSettingsDetails d) {
        String red     = d.getHueRed();
        String orange  = d.getHueOrange();
        String yellow  = d.getHueYellow();
        String green   = d.getHueGreen();
        String aqua    = d.getHueAqua();
        String blue    = d.getHueBlue();
        String purple  = d.getHuePurple();
        String magenta = d.getHueMagenta();

        if(allNull(red, orange, yellow, green, aqua, blue, purple, magenta)) { return null; }

        return format(
            "Hue: Red(%s) Orange(%s) Yellow(%s) Green(%s) Aqua(%s) Blue(%s) Purple(%s) Magenta(%s)",
            either(red, "0"),
            either(orange, "0"),
            either(yellow, "0"),
            either(green, "0"),
            either(aqua, "0"),
            either(blue, "0"),
            either(purple, "0"),
            either(magenta, "0")
        );
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getColorSaturation(DevelopSettingsDetails d) {
        String red     = d.getSaturationRed();
        String orange  = d.getSaturationOrange();
        String yellow  = d.getSaturationYellow();
        String green   = d.getSaturationGreen();
        String aqua    = d.getSaturationAqua();
        String blue    = d.getSaturationBlue();
        String purple  = d.getSaturationPurple();
        String magenta = d.getSaturationMagenta();

        if(allNull(red, orange, yellow, green, aqua, blue, purple, magenta)) { return null; }

        return format(
            "Saturation: Red(%s) Orange(%s) Yellow(%s) Green(%s) Aqua(%s) Blue(%s) Purple(%s) Magenta(%s)",
            either(red, "0"),
            either(orange, "0"),
            either(yellow, "0"),
            either(green, "0"),
            either(aqua, "0"),
            either(blue, "0"),
            either(purple, "0"),
            either(magenta, "0")
        );
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getColorLuminance(DevelopSettingsDetails d) {
        String red     = d.getLuminanceRed();
        String orange  = d.getLuminanceOrange();
        String yellow  = d.getLuminanceYellow();
        String green   = d.getLuminanceGreen();
        String aqua    = d.getLuminanceAqua();
        String blue    = d.getLuminanceBlue();
        String purple  = d.getLuminancePurple();
        String magenta = d.getLuminanceMagenta();

        if(allNull(red, orange, yellow, green, aqua, blue, purple, magenta)) { return null; }

        return format(
            "Luminance: Red(%s) Orange(%s) Yellow(%s) Green(%s) Aqua(%s) Blue(%s) Purple(%s) Magenta(%s)",
            either(red, "0"),
            either(orange, "0"),
            either(yellow, "0"),
            either(green, "0"),
            either(aqua, "0"),
            either(blue, "0"),
            either(purple, "0"),
            either(magenta, "0")
        );
    }

    // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    private static void printSplitTone(DevelopSettingsDetails d) {
        List<String> lines = new ArrayList<>();

        add(lines, getSplitToneHighlights(d));
        add(lines, getSplitToneBalance(d));
        add(lines, getSplitToneShadows(d));

        printSection("Split Toning", lines);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getSplitToneHighlights(DevelopSettingsDetails d) {
        String hue = d.getSplitToneHighlightHue();
        String saturation = d.getSplitToneHighlightSaturation();

        if(allNull(hue, saturation)) { return null; }

        return format(
            "Highlights: Hue(%s) Saturation(%s)",
            either(hue, "0"),
            either(saturation, "0")
        );
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getSplitToneBalance(DevelopSettingsDetails d) {
        String balance = d.getSplitToneBalance();

        if(allNull(balance)) { return null; }

        return format(
            "Balance(%s)",
            either(balance, "0")
        );
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getSplitToneShadows(DevelopSettingsDetails d) {
        String hue = d.getSplitToneShadowHue();
        String saturation = d.getSplitToneShadowSaturation();

        if(allNull(hue, saturation)) { return null; }

        return format(
            "Shadows: Hue(%s) Saturation(%s)",
            either(hue, "0"),
            either(saturation, "0")
        );
    }

    // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    private static void printDetail(DevelopSettingsDetails d) {
        List<String> lines = new ArrayList<>();

        add(lines, getSharpening(d));
        add(lines, getLuminanceNoiseReduction(d));
        add(lines, getColorNoiseReduction(d));

        printSection("Detail", lines);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getSharpening(DevelopSettingsDetails d) {
        String amount = d.getSharpeningAmount();
        String radius = d.getSharpeningRadius();
        String detail = d.getSharpeningDetail();
        String masking = d.getSharpeningMasking();

        if(allNull(amount, radius, detail, masking)) { return null; }

        return format(
            "Sharpening: Amount(%s) Radius(%s) Detail(%s) Masking(%s)",
            either(amount, "0"),
            either(radius, "0.0"),
            either(detail, "0"),
            either(masking, "0")
        );
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getLuminanceNoiseReduction(DevelopSettingsDetails d) {
        String amount = d.getNoiseReductionLuminance();
        String detail = d.getNoiseReductionLDetail();
        String contrast = d.getNoiseReductionContrast();

        if(allNull(amount, detail, contrast)) { return null; }

        return format(
            "Noise Reduction: Luminance(%s) Detail(%s) Contrast(%s)",
            either(amount, "0"),
            either(detail, "0"),
            either(contrast, "0")
        );
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getColorNoiseReduction(DevelopSettingsDetails d) {
        String amount = d.getNoiseReductionColor();
        String detail = d.getNoiseReductionCDetail();
        String smooth = d.getNoiseReductionSmooth();

        if(allNull(amount, detail, smooth)) { return null; }

        return format(
            "Noise Reduction: Color(%s) Detail(%s) Smoothing(%s)",
            either(amount, "0"),
            either(detail, "0"),
            either(smooth, "0")
        );
    }

    // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

    private static void printEffects(DevelopSettingsDetails d) {
        List<String> lines = new ArrayList<>();

        add(lines, getGrain(d));
        add(lines, getDehaze(d));

        printSection("Effects", lines);
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getGrain(DevelopSettingsDetails d) {
        String amount = d.getGrainAmount();
        String size = d.getGrainSize();
        String rough = d.getGrainRoughness();

        if(allNull(amount, size, rough)) { return null; }

        if(amount == null && size.equals("25") && rough == null) { return null; }

        return format(
            "Grain: Amount(%s) Size(%s) Roughness(%s)",
            either(amount, "0"),
            either(size, "0"),
            either(rough, "0")
        );
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private static String getDehaze(DevelopSettingsDetails d) {
        String amount = d.getEffectsDehaze();

        if(allNull(amount)) { return null; }

        return format(
            "Dehaze: Amount(%s)",
            either(amount, "0")
        );
    }
}
