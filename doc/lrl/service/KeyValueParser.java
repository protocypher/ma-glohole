package com.smt.lrl.service;

import java.util.HashMap;
import java.util.Map;

public class KeyValueParser {

    public static Map<String,String> parse(final String text) {
        Map<String,String> lr = new HashMap<>();

        int open = text.indexOf('{') + 1;
        int close = text.lastIndexOf('}') - 1;
        String normalized = text.substring(open, close).replaceAll("\\s+", " ").trim();

        String[] tokens = normalized.split(" = ");
        String value, key = tokens[0].trim();
        int index;

        for(int i = 1; i < tokens.length - 1; i++) {
            index = tokens[i].lastIndexOf(',');
            if(index < 0) { continue; }
            value = tokens[i].substring(0, index).trim();
            lr.put(key, value);
            key = tokens[i].substring(index + 1).trim();
        }

        lr.put(key, tokens[tokens.length - 1].trim());

        return lr;
    }
}
